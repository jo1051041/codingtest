> # 문제 설명

수많은 마라톤 선수들이 마라톤에 참여하였습니다. 단 한 명의 선수를 제외하고는 모든 선수가 마라톤을 완주하였습니다.

마라톤에 참여한 선수들의 이름이 담긴 배열 participant와 완주한 선수들의 이름이 담긴 배열 completion이 주어질 때, 완주하지 못한 선수의 이름을 return 하도록 solution 함수를 작성해주세요.

> # 제한 사항

- 마라톤 경기에 참여한 선수의 수는 1명 이상 100,000명 이하입니다.
- completion의 길이는 participant의 길이보다 1 작습니다.
- 참가자의 이름은 1개 이상 20개 이하의 알파벳 소문자로 이루어져 있습니다.
- 참가자 중에는 동명이인이 있을 수 있습니다.

> # 입출력 예제

|participant	|completion	|return|
|:-------------:|:---------:|:----:|
|["leo", "kiki", "eden"]	|["eden", "kiki"]	|"leo"|
|["marina", "josipa", "nikola", "vinko", "filipa"]	|["josipa", "filipa", "marina", "nikola"]|	"vinko"|
|["mislav", "stanko", "mislav", "ana"]	|["stanko", "ana", "mislav"]	|"mislav"|

> # 해설

>> ## JAVA

- ArrayList에 참가자 목록을 담은 후 완주자 명단에 존재하는 선수를 ArrayList에서 remove 처리
- 단, 완주다 명단과 참가자 목록에 1대1 방식으로 remove 처리 (동명이인까지 함께 remove 처리 방지 필요)
- 배열을 `ArrayList`로 형태 변환하는 메소드
  - 참고 자료 : https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/util/Arrays.html#asList(T...)
  ```java
  ArrayList<String> list = new Arrays.asList([배열]);
  ```

```java
import java.util.ArrayList;
import java.util.Arrays;

class Solution {
    public String solution(String[] participant, String[] completion) {
        String answer = "";
       
        ArrayList<String> list = new ArrayList<String>(Arrays.asList(participant));
        
        for(int i=0; i<completion.length; i++) {
        	if(list.contains(completion[i])) {
        		list.remove(list.indexOf(completion[i]));
        	}
        }
        
        answer = list.get(0);
        
        return answer;
    }
}
```
